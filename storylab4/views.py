from django.shortcuts import render
from .forms import CreateJadwal
from django.http import HttpResponse
from .models import Schedule
from django.http import HttpResponseRedirect

def index(request):
	return render (request, "STORY3.html")
	
def about(request):
	return render (request, "STORY3_1.html")

def website(request):
	return render (request, "STORY3_2.html")
	
def project(request):
	return render (request, "STORY3_3.html")
	
def navbar(request):
	return render (request, "navbar.html")
	
def AddSchedule(request):
    response = {'create_jadwal': CreateJadwal}
    return render(request, 'AddSchedule.html', response)

def SaveSchedule(request):
    response = {'save_status':''}
    if (request.method == 'POST'):
        form = CreateJadwal(request.POST)
        if (form.is_valid()):
            date_hours = form.cleaned_data['date_hours']
            day = date_hours.strftime('%A')
            activity_name = form.cleaned_data['activity_name']
            place = form.cleaned_data['place']
            category = form.cleaned_data['category']
            
            schedule = Schedule(
				date_hours = date_hours,
				day = day,
				activity_name = activity_name,
				place = place,
				category = category
            )

            schedule.save()

            response['save_status'] = 'Schedule has successfully been added'
        else:
            response['save_status'] = 'Oops, something went wrong and you cannot add this schedule. Check whether you have inputed everything correctly or not.'
    else:
        response['save_status'] = 'Oops, something went wrong!'

    return render(request, 'SaveSchedule.html', response)

def ViewSchedule(request):
    schedules = Schedule.objects.all().values()
    response = {'schedules':schedules}
    return render(request, 'ViewSchedule.html', response)

def ClearSchedule(request, schedule_id = None):
    schedules = Schedule.objects.all().values()
    Schedule.objects.get(id=schedule_id).delete()
    response = {'schedules':schedules}
    return HttpResponseRedirect('/storylab4/view_schedule/')
