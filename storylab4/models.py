from django.db import models
from datetime import date

class Schedule(models.Model):
    day = models.CharField(max_length=50)
    date_hours = models.DateTimeField()
    activity_name = models.CharField(max_length=50)
    place = models.CharField(max_length=50)
    category = models.CharField(max_length=50)

