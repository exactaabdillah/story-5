from django import forms
from .models import Schedule

class CreateJadwal(forms.Form):
    date_hours_attrs = {
        'type': 'date',
        'class': 'form-control',
    }

    activity_attrs = {
        'type':'text',
        'class': 'form-control',
        'placeholder':'Activity...'
    }

    place_attrs = {
        'type':'text',
        'class': 'form-control',
        'placeholder':'Place...'
    }

    category_attrs = {
        'type':'text',
        'class': 'form-control',
        'placeholder':'Category Name....'
    }
	
    date_hours = forms.DateTimeField(label='Date and Time', required=True, localize=True, widget=forms.DateInput(attrs=date_hours_attrs))
    activity_name = forms.CharField(label='Activity', max_length=50,  required=True, widget=forms.TextInput(attrs=activity_attrs))
    place = forms.CharField(label='Place', max_length=50, required=True, widget=forms.TextInput(attrs=place_attrs))
    category = forms.CharField(label='Category Name', max_length=50, required=True, widget=forms.TextInput(attrs=category_attrs))

def label_from_instance(self, obj):
    return "{}: {}, {}".format(obj.activity_name, obj.day, obj.date_hours)
	