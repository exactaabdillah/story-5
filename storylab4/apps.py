from django.apps import AppConfig


class Storylab4Config(AppConfig):
    name = 'storylab4'
